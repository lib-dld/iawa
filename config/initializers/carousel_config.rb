Rails.application.configure do
  config.carousel_images = %w{
    iawa_logo.jpg 
    IAWADonationLetter_1985_p1.jpg
    Ms2013-089_SuzukiKimiko_ArchitecturalDrawing_nd_430328.jpg
    Ms2001-005_CurrieVirginia_WatercolorInteriorDesign_nd_a.jpg
  }
end
